# -*- coding: utf-8 -*-
"""
Copyright (c) 2013,
Franziska Zickmann, 
ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
Distributed under the GNU Lesser General Public License, version 3.0
"""
#!/usr/bin/env python
import os
import optparse

parser = optparse.OptionParser()

args = parser.parse_args()

fastqFiles = args[1][0]
output = args[1][1]

fileArr = fastqFiles.split("&&")

fileNames = ""

for file in fileArr:
	fileNames = "%s %s" % (fileNames,file)

syscall = "cat %s > %s" %(fileNames,output)
os.system(syscall)
