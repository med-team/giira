# -*- coding: utf-8 -*-
"""
Created on Feb 03 2012
helper script to call bwa index 
Copyright (c) 2013,
Franziska Zickmann, 
ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
Distributed under the GNU Lesser General Public License, version 3.0
"""
#!/usr/bin/env python
import os
import optparse
import subprocess

parser = optparse.OptionParser()

args = parser.parse_args()

orfFile = args[1][0]
print orfFile
para = args[1][1]
print para
fnull = open(os.devnull, 'w')

syscall = "bwa index -a %s %s" %(para, orfFile)
print syscall
result = subprocess.call(syscall, shell = True, stdout = fnull, stderr = fnull)
fnull.close()
