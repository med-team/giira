# -*- coding: utf-8 -*-
"""
Copyright (c) 2013,
Franziska Zickmann, 
ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
Distributed under the GNU Lesser General Public License, version 3.0
"""
#!/usr/bin/env python
import os
import sys

pathOut = sys.argv[1]

syscall1 = "samtools view -b -S %sresultsRun/reassignedReads.sam > %sresultsRun/reassignedReads.bam" %(pathOut,pathOut)
print syscall1
os.system(syscall1)

syscall2 = "samtools sort -n %sresultsRun/reassignedReads.bam %sresultsRun/reassignedReads_sorted" %(pathOut,pathOut)
print syscall2
os.system(syscall2)

syscall3 = "samtools view -h -o %sresultsRun/reassignedReads_sorted.sam %sresultsRun/reassignedReads_sorted.bam" %(pathOut,pathOut)
print syscall3
os.system(syscall3)