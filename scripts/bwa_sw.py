# -*- coding: utf-8 -*-
"""
Created on Oct 31 16:29:55 2011
helper script to call bwa samse
Copyright (c) 2013,
Franziska Zickmann, 
ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
Distributed under the GNU Lesser General Public License, version 3.0
"""
#!/usr/bin/env python
import os
import optparse
import subprocess

parser = optparse.OptionParser()

args = parser.parse_args()

paras = args[1][0]
orfFile = args[1][1]
rnaFile = args[1][2]
outFile = args[1][3]

callPara = " "
fnull = open(os.devnull, 'w')

if paras == 0:
        syscall = "bwa bwasw %s %s > %s" %(orfFile,rnaFile,outFile)
        #os.system(syscall)
        result = subprocess.call(syscall, shell = True, stdout = fnull, stderr = fnull)
        fnull.close()
else:
        stringAr = paras[1:(len(paras)-1)].split("_")
        for data in stringAr:
                callPara=callPara+data+" "
        syscall = "bwa bwasw%s %s %s> %s" %(callPara,orfFile,rnaFile,outFile)
        #os.system(syscall)
        print syscall
        result = subprocess.call(syscall, shell = True, stdout = fnull, stderr = fnull)
        fnull.close()


