'''
Created on 4 November 2013

calls the glpsol lp solver to assign the rnas

@author: zickmannf
'''

import sys
import numpy
import os

inputFile = sys.argv[1]
outputFile = sys.argv[2]

syscall = "glpsol --lp %s --output %s" %(inputFile,outputFile)
os.system(syscall)
