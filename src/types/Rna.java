package types;

/**
 * rna object to store quality and other important stuff 
 * Copyright (c) 2013,
 * Franziska Zickmann, 
 * ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
 * Distributed under the GNU Lesser General Public License, version 3.0
 *
 */

import java.util.Vector;

public class Rna {

	public String rnaID;	// key		
	
	public double quality; 	  	

	public Vector<Object[]> contigsMappedOn = new Vector<Object[]>(); // contains several Arrays ala: [contig, alignPos, cigarString, mapQual,spliceInfo,mismatchInfo,direcInfo] (one for each hit)
	
	public int isMulti;		// indicator if this read is an ambiguous read
	
	public int hitNum;	// the original number of hits
	
	public int assignedNum;  // shows the number of assignments to a candidate
	
	public Vector<Integer> isSharedBy = new Vector<Integer>();  // necessary for twin clusters -> contains the ids of all clusters that share this rna

}
