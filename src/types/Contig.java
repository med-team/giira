package types;

import java.util.TreeMap;
import java.util.Vector;

/**
 * stores contigs and their mapped positions, splicing information and assigned genes
 * Copyright (c) 2013,
 * Franziska Zickmann, 
 * ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
 * Distributed under the GNU Lesser General Public License, version 3.0
 *
 */

public class Contig {

	public String contigName;	// key
	
	public TreeMap<Integer,Vector<Rna>> positionTOmappingRnas = new TreeMap<Integer,Vector<Rna>>();	// such that we can traverse all mapped positions
	
	public Vector<Gene> allGenes = new Vector<Gene>();	// stores all assigned gene candidates of this contig
	
	public TreeMap<Integer ,Integer> splicePositions = new TreeMap<Integer,Integer>();  // first entry: position in contig; second entry: number of reads 
	
	public TreeMap<Integer ,Integer> positionTOdiff = new TreeMap<Integer,Integer>();  // remembers read differences, needed for insertions + deletions, coverage increased/decreased according to value
	
}