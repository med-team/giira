package types;

/**
 * all properties of genes identified by the gene finder
 * Copyright (c) 2013,
 * Franziska Zickmann, 
 * ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
 * Distributed under the GNU Lesser General Public License, version 3.0
 */

import java.util.HashMap;
import java.util.TreeMap;
import java.util.Vector;

public class Gene {
	
	public int geneID;	// key
	
	public String sequence = new String();  // interval determined by start and stop codon if this candidate gene, introns are included
	public Vector<int[]>  operonOrfs = new Vector<int[]>();  // only for prokaryotes, contains operon orfs
	public boolean operonDirectionIsForward;  // true if forward
	
	public int exonLength;					// sum of all exon lengths

	public double score;					// score depending on assigned reads, reflect probability of this candidate to be a true gene
	
	public int startPos;					// position of start codon
	public int[] possibleStarts_Forward = {-1,-1,-1};  // each gene has 6 possible starts (3 for each strand) within a specified interval
	public int[] possibleStarts_Reverse = {-1,-1,-1};
	
	public int stopPos;						// position of stop codon
	public int[] possibleStops_Forward = {-1,-1,-1};	  // each gene has 6 possible stops (3 for each strand) within a specified interval
	public int[] possibleStops_Reverse = {-1,-1,-1};
	
	public int coreSeq;  // if the core is only as big as the read length, this gene is penalized, because it is very likely to be a false positive
	
	public TreeMap<Integer,Object[]> possibleIntrons = new TreeMap<Integer,Object[]>(); // first entry: Vector<int[]>, second entry: Vector<Vector<Rna>> -> introns of this site 
																						// with supporting rnas; third entry: exon end if rnas exist not supporting this split, -1 else ; fourth entry: starting site for "fake splits"
	
	public TreeMap<Integer,Vector<Rna>> possibleFussyExons = new TreeMap<Integer,Vector<Rna>>();  // stores all rnas that support a "fussy exon" where reads do not support the splice site
	
	public Vector<int[]> exonsOfGene = new Vector<int[]>();  // contains all exons of this gene
	
	public Vector<Object[]> alternativeTranscripts = new Vector<Object[]>();  // if this gene has alternative transcripts, these are defined by one intron, which is stored here
	public Vector<int[]> eraseIntrons_temp = new Vector<int[]>();
	public Vector<int[]> intronEndsThatAreNotContinued = new Vector<int[]>(); // if this intron end leads to an exon that is not continued, store this here to remember for exonSearch

	public double uniqueCov;	// coverage of this gene only by unique reads
	public double totalCov;     // coverage of this gene given by all assigned reads
	public int numOfMultis;		// number of ambiguous reads mapping this gene
	public boolean hasStop_temp;	// indicator for candidate extraction, sign if the gene did not find a stop during the first search
	
	public boolean onRevStrand;  // indicates whether ORF is on forward or reverse strand
	
	public boolean realDirectionNotKnown;		// allow a gene to switch the direction if necessary
	
	public HashMap<String,Object[]> idTOassociatedRnas = new HashMap<String,Object[]>();  // Object[0] = rna,[1] = supportedSplit (int[] with spliceKey and intronEnd (-1,-1) if not present), [2] = supportedFussyExon (int or -1)
	
	public int[] direcCounter = {0,0};  // counts the reads supporting forward [0] and reverse [1] strand, only possible if XS tag provided
	
	public Vector<String> moreThanOneHitRnas = new Vector<String>();    // if a read maps more than once to this gene, add its id here as a backup
	
	public Gene twinNode;
	public boolean freeToResolve;  //indicates if this cluster and its twin can be locally resolved, if false, this is only possible during the ambiguous read optimization
	
	public boolean hadTwinBefore;  // if true we have to mark the associated transcript as one to be 6-frame translated (because there is an uncertainty which strand)
	
	public boolean isMergedTwin;	     // indicates the twin that has been merged with other clusters (if this has happened only to one twin)
	
	/////////////// FOR RNA-ProGen //////////////////////
	public Vector<Vector<int[]>> exonsOfTranscripts = new Vector<Vector<int[]>>();
	public boolean wasLookedAt;   // necessary for score update
	public double scoreRnaPart;
}
