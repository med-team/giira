package geneFinder;

import java.io.File;
import java.io.IOException;


/**
 * concatenates different genome files to one reference file
 * Copyright (c) 2013,
 * Franziska Zickmann, 
 * ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
 * Distributed under the GNU Lesser General Public License, version 3.0
 *
 */

public class PrepareMapping_GF {

	/*
	 * concatenate the reference files to one large fasta
	 */
	
	
	public String prepareRefFile_GF(){

		String nameRef = GeneFinder.pathOut+"concaRefFile";

		Runtime prepRef = Runtime.getRuntime();
		Process firstExe;
		String allRefNames = new String();

		try {

			for(File genomeFile : GeneFinder.genomeFilesWithNames.keySet()){
				String name = GeneFinder.pathToGenomeFiles+GeneFinder.genomeFilesWithNames.get(genomeFile);
				allRefNames += name+"&&";
			}

			if(GeneFinder.useTopHat){
				nameRef += ".fa";
			}else{
				nameRef += ".fasta";
			}

			String exe = "python "+ GeneFinder.pathToHelpFiles+"callCat.py " + allRefNames + " " + nameRef;
			firstExe = prepRef.exec(exe);
			firstExe.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return (GeneFinder.pathOut+"concaRefFile");
	}
	
}
