package geneFinder;

/**
 * call TopHat to map the rna reads against the reference genome 
 * Copyright (c) 2013,
 * Franziska Zickmann, 
 * ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
 * Distributed under the GNU Lesser General Public License, version 3.0
 * 
 */

import java.io.File;

public class TopHat_Call {

	/*
	 * map the reads against the reference using TopHat2 
	 * creates a sam output file
	 * note: topHat setting is only possible for the mapping step, not for the indexing with bowtie-build
	 */
	
	public void callTopHat(String nameRefFile){	

		System.out.println("Start to do the alignment with TopHat...");
		WriteOutput.writeToLogFile("Start to do the alignment with TopHat... ");


		long timebef = System.currentTimeMillis();
		long timeAfter = 0;

		// first build a bowtie index on the reference file

		File refFile;
		refFile = new File(nameRefFile+".fa");

		System.out.print("Indexing with Bowtie.... ");
		String firstExe = "bowtie2-build " + refFile + " " + nameRefFile;
		Giira.callAndHandleOutput(firstExe,true);

		String optionString = "";   // get the options for the alignment
		if(GeneFinder.settingMapper != null){
			for(String para : GeneFinder.settingMapper.split(",")){   // comma separated list
				String[] paraArr = para.split("=");
				if(paraArr.length == 1){		// if length == 1, than this parameter has only one key as indicator
					optionString = optionString+paraArr[0]+" ";
				} else{
					optionString = optionString+paraArr[0]+" "+paraArr[1]+" ";
				}
			}
		}

		System.out.println("Done. \nPerform alignment.... ");

		if(optionString.length() > 1){	// report the options
			System.out.println("Options for TopHat: " + optionString);
			WriteOutput.writeToLogFile("Options for TopHat: " + optionString + "\n");
		}

		// now call topHat, note that we do use the bam format as an intermediate format to ensure the right ordering of reads, final output is in sam format

		String out_dir = GeneFinder.pathOut;

		String fileNames = new String();

		File rnaFile = null;

		for(File readFile : GeneFinder.rnaFilesWithNames.keySet()){     // if there is more than one read file, report them to tophat in a list
			fileNames += (GeneFinder.rnaFilesWithNames.get(readFile))+",";
			if(GeneFinder.rnaFilesWithNames.keySet().size() == 1){
				rnaFile = readFile;
			}
		}

		fileNames=fileNames.substring(0,fileNames.length()-1);  // trim to correct format

		String secondExe = "";

		if(GeneFinder.rnaFilesWithNames.keySet().size() == 1){	// make the right call depending on how many read files are provided
			secondExe = "tophat2 --no-sort-bam " + optionString + "-o " + out_dir + " " + nameRefFile + " " + rnaFile;
			Giira.callAndHandleOutput(secondExe,true);

		}else{
			secondExe = "tophat2 --no-sort-bam " + optionString + "-o " + out_dir + " " + nameRefFile + " " + fileNames;
			Giira.callAndHandleOutput(secondExe,true);			
		}

		// the following is necessary to ensure that the reads in the resulting sam file will be in the necessary order
		String thirdExe = "samtools sort -n " + out_dir + "/accepted_hits.bam " + out_dir + "/accepted_hits_sorted";  // sort and view guarantees that sam file is sorted correctly
		String fourthExe = "samtools view -h -o " + out_dir + "/accepted_hits.sam " + out_dir + "/accepted_hits_sorted.bam";  // accepted_hits.sam is the file for the further analysis

		Giira.callAndHandleOutput(thirdExe,true);
		Giira.callAndHandleOutput(fourthExe,true);

		// log messages
		System.out.println("Done.");	    	
		timeAfter = System.currentTimeMillis();	    
		System.out.println("Time required for the alignment: "+ (double) (timeAfter-timebef)/1000.0 +"s.");			
		WriteOutput.writeToLogFile("Done.\n Time required for the alignment: "+ (double) (timeAfter-timebef)/1000.0 +"s.\n\n");

	}
	
}
