package geneFinder;

import types.*;

/**
 * contains methods to make sure, that extracted twin clusters and can be treated locally (no relationship to neighboring clusters) are 
 * resolved before starting the ambiguous reads optimization
 * Copyright (c) 2013,
 * Franziska Zickmann, 
 * ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
 * Distributed under the GNU Lesser General Public License, version 3.0
 *
 */

public class LocalTwinResolve {
	
	/*
	 * takes two twins and their assigned reads and decides whether the forward, or the reverse twin shall live
	 * note: at the moment only consider exon length
	 */
	
	public static Gene resolveTwins(Gene cluster){
		
		Gene twin = cluster.twinNode;

		cluster.exonLength = (cluster.stopPos-cluster.startPos +1);
		twin.exonLength = (twin.stopPos-twin.startPos +1);
		
		if(cluster.exonLength <= twin.exonLength){
			// return the cluster
			undeclareShared(cluster);
			cluster.twinNode = null;
					
			cluster.exonsOfGene.clear();
			cluster.exonLength = 0;
			
			return cluster;
		}else{
			// return the twin
			undeclareShared(twin);
			twin.twinNode = null;
			
			twin.exonsOfGene.clear();
			twin.exonLength = 0;
			
			return twin;
		}
		
	}
	
	/*
	 * when we removed one twin cluster, declare all the rnas of the remaining one as "not shared"
	 */
	
	public static void undeclareShared(Gene cluster){
		
		for(String rnaKey : cluster.idTOassociatedRnas.keySet()){	
			Rna rna = ((Rna) cluster.idTOassociatedRnas.get(rnaKey)[0]);
			rna.isSharedBy.removeElement(cluster.geneID);
			rna.isSharedBy.removeElement(cluster.twinNode.geneID);
		}
	}
	
}
