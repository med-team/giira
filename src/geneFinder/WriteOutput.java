package geneFinder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import types.*;

/**
 * write the result of the gene finding procedure to several output files
 * Copyright (c) 2013,
 * Franziska Zickmann, 
 * ZickmannF@rki.de, Robert Koch-Institute, Berlin, Germany
 * Distributed under the GNU Lesser General Public License, version 3.0
 *
 */

public class WriteOutput {

	public static int geneNumTotal;

	/*
	 * opens the log file and appends the given string
	 */

	public static void writeToLogFile(String text){

		try {
			FileWriter log = new FileWriter(GeneFinder.logFile,true);
			log.write(text);
			log.close();
		} catch (FileNotFoundException r) {
			r.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * opens the sam file containing the reassigned hits and appends the given string
	 */

	public static void writeToOtherFile(String filename, String text){

		try {
			FileWriter f = new FileWriter(filename,true);
			f.write(text);
			f.close();
		} catch (FileNotFoundException r) {
			r.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * sorts the sam file containing the reassigned hits (according to read names)
	 */

	public static void sortReassignSamFile(){

		String exe = "python " + GeneFinder.pathToHelpFiles+"sortReaSam.py " + GeneFinder.pathOut;
		Giira.callAndHandleOutput(exe,true);

	}

	/*
	 * opens the sam file containing the reassigned hits and appends the given string
	 */

	public static void removeReassignSamFile(){

		try {		
			Runtime removeFile = Runtime.getRuntime();
			Process exe3 = removeFile.exec("rm " + GeneFinder.pathOut+"reassignedReads.bam");
			exe3.waitFor();	
			Process exe4 = removeFile.exec("rm " + GeneFinder.pathOut+"reassignedReads_sorted.bam");
			exe4.waitFor();
		} catch (InterruptedException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * created a fasta file with all gene sequences and a bed file with all statistics like score, start, stop etc.
	 */

	public static void writeGeneFinderOutput(double[] minMax, String namePartOut){

		try{

			BufferedWriter bwGTF = null;

			if(GeneFinder.iteration == 2){
				bwGTF = new BufferedWriter(new FileWriter(new File(GeneFinder.pathOut+"stats"+ GeneFinder.outputName + "_final.gtf")));		
			}else{				
				bwGTF = new BufferedWriter(new FileWriter(new File(GeneFinder.pathOut+"stats"+ GeneFinder.outputName + namePartOut + ".gtf")));
			}

			int exonLength_covered_total = 0;
			geneNumTotal = 0;
			int geneRefNonZero = 0;

			int numGenesWithOnlyMulti = 0;
			int numGenesWithSupportBelowThreshold = 0;
			int numGenesWithTotalSupportBelowThreshold = 0;

			for(String contigName : GeneFinder.mappedContigs.keySet()){
				Contig thisContig = GeneFinder.mappedContigs.get(contigName);

				for(Gene cluster : thisContig.allGenes){

					geneNumTotal++;

					if(cluster.score != 0){						

						cluster.score = (cluster.score/( minMax[1]- minMax[0] + 1 ) );  // normalization

						String multiInfo = "y";  // y stands for non-multi support, n stands for only multi support
						String supportInfo = "y";  // yes stands for sufficient total support, no for not sufficient support
						String hasEnoughUniques = "y";  // is n, if proportion of unique reads is below 1% and also unique coverage

						if(cluster.numOfMultis == cluster.idTOassociatedRnas.keySet().size()){
							numGenesWithOnlyMulti++;
							multiInfo = "n";

						}else{
							if(GeneFinder.noAmbiOpti){
								if(((((double) cluster.idTOassociatedRnas.size()) * (double) GeneFinder.readLength)/( (double) cluster.exonLength)) < GeneFinder.minCoverage){
									numGenesWithSupportBelowThreshold++;
								}
							}else{
								if(cluster.uniqueCov < GeneFinder.minCoverage){
									numGenesWithSupportBelowThreshold++;
								}
							}

						}

						if(((((double) cluster.idTOassociatedRnas.size()) * (double) GeneFinder.readLength)/( (double) cluster.exonLength)) < GeneFinder.minCoverage){
							numGenesWithTotalSupportBelowThreshold++;
							supportInfo = "n";
						}

						double coverage = ((((double) cluster.idTOassociatedRnas.size()) * (double) GeneFinder.readLength)/( (double) cluster.exonLength));
						
						int uniqueNum = (cluster.idTOassociatedRnas.keySet().size() - cluster.numOfMultis);

						if((((double)uniqueNum)/((double)cluster.idTOassociatedRnas.keySet().size()) < 0.1)){ // proportion of multi reads
							if((((double) uniqueNum) * (double) GeneFinder.readLength)/((double) cluster.exonLength) < 0.1){  // uniqueCov
								hasEnoughUniques = "n";
							}
						}

						geneRefNonZero++;
						exonLength_covered_total += cluster.exonLength; 

						String strandInfo = "+";   // has to be updated if reverse strand

						if(cluster.onRevStrand){
							strandInfo = "-";
						}

						boolean onlyNewTranscriptStarts = DefineAlternativeTranscripts.searchForTranscripts(cluster);

						if(cluster.alternativeTranscripts.isEmpty()){
							// first write out transcript
							bwGTF.write(contigName + "\tGIIRA\ttranscript\t" + (cluster.startPos+1) + "\t" + (cluster.stopPos+1) + 
									"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

							// now write out all exons

							int exonNum = 1;
							for(int[] exon : cluster.exonsOfGene){
								bwGTF.write(contigName + "\tGIIRA\texon\t" + (exon[0]+1) + "\t" + exon[1] + 
										"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; exon_number " + (exonNum++) + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");
							}
						}else{
							int transcriptID = 1;
							
							if(cluster.possibleIntrons.keySet().size() == 0 || onlyNewTranscriptStarts){
								// only alternative transcripts because of new transcripts starts, add the start to ensure that we grab all exons in at least one alternative
								cluster.alternativeTranscripts.add(new Object[]{cluster.startPos,cluster.startPos,cluster.startPos,cluster.startPos});
							}

							FindExonsOfGene.sortExons(cluster);
							DefineAlternativeTranscripts.eraseEqualTranscripts(cluster);

							for(Object[] definingIntron : cluster.alternativeTranscripts){

								if(!(((Integer) definingIntron[0] == (Integer) definingIntron[1]) && (definingIntron.length >= 6) && ((Integer)definingIntron[4] == -1))){
									Object[] returnObject = DefineAlternativeTranscripts.assignExonsToTranscripts(cluster,definingIntron);

									Vector<int[]> exonsOfTranscript = (Vector<int[]>) returnObject[0];
									cluster.exonsOfTranscripts.add(exonsOfTranscript);

									int transcriptStart = (Integer) returnObject[1];
									int transcriptEnd = (Integer) returnObject[2];
									if(!exonsOfTranscript.isEmpty()){
										bwGTF.write(contigName + "\tGIIRA\ttranscript\t" + (transcriptStart+1) + "\t" + (transcriptEnd) + 
												"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + "." + transcriptID + "\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

										int exonNum = 1;
										for(int[] exon : exonsOfTranscript){
											bwGTF.write(contigName + "\tGIIRA\texon\t" + (exon[0]+1) + "\t" + exon[1] + 
													"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + "." + transcriptID + "\"; exon_number " + (exonNum++) + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");
										}

										transcriptID++;
									}

								}

							}
						}

					}
				}

				if(GeneFinder.iteration == 1 && !GeneFinder.inprogeaCall){
					thisContig.allGenes = new Vector<Gene>();			
				}

			}

			//System.out.println("Number of identified genes with support below threshold: " +  numGenesWithSupportBelowThreshold);
			System.out.println("Number of identified genes with total coverage below threshold: " +  numGenesWithTotalSupportBelowThreshold);
			System.out.println("Number of identified genes with only ambiguous support: " +  numGenesWithOnlyMulti);
			System.out.println("Number of identified genes on reference: " +  geneRefNonZero);

			writeToLogFile("\n\nNumber of identified genes on reference: " +  geneRefNonZero + " \nNumber of identified genes with total coverage below threshold: " +  numGenesWithTotalSupportBelowThreshold + "\nNumber of identified genes with only ambiguous support: " +  numGenesWithOnlyMulti + "\n");

			bwGTF.close();

		}catch (FileNotFoundException r) {
			r.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * output for prokaryotes
	 */

	public static void writeGeneFinderOutput_Prokaryote(double[] minMax, String namePartOut){

		try{

			BufferedWriter bwGTF_ccAna = null;
			BufferedWriter bwGTF_prok = null;

			bwGTF_ccAna = new BufferedWriter(new FileWriter(new File(GeneFinder.pathOut+"stats"+ GeneFinder.outputName + namePartOut +"_ccAna.gtf")));
			bwGTF_prok = new BufferedWriter(new FileWriter(new File(GeneFinder.pathOut+"stats"+ GeneFinder.outputName + namePartOut +"_prok.gtf")));

			int exonLength_covered_total = 0;
			geneNumTotal = 0;
			int geneRefNonZero = 0;

			int numGenesWithOnlyMulti = 0;
			int numGenesWithSupportBelowThreshold = 0;
			int numGenesWithTotalSupportBelowThreshold = 0;
			int noCistron = 0;
			int moreThanOneCistron = 0;
			int moreThanTwoCistrons = 0;
			int moreOperons = 0;
			int operonSplitNum = 0;
			double operonSplitAv = 0.0;

			for(String contigName : GeneFinder.mappedContigs.keySet()){
				Contig thisContig = GeneFinder.mappedContigs.get(contigName);

				for(Gene cluster : thisContig.allGenes){

					geneNumTotal++;

					if(cluster.score != 0){						

						cluster.score = (cluster.score/( minMax[1]- minMax[0] + 1 ) );  // normalization

						String multiInfo = "y";  // n stands for non-multi support, y stands for only multi support
						String supportInfo = "y";  // yes stands for sufficient total support, no for not sufficient support
						String hasEnoughUniques = "y";  // is n, if proportion of unique reads is below 1% and also unique coverage

						if(cluster.numOfMultis == cluster.idTOassociatedRnas.keySet().size()){
							numGenesWithOnlyMulti++;
							multiInfo = "n";

						}else{
							if(GeneFinder.noAmbiOpti){
								if(((((double) cluster.idTOassociatedRnas.size()) * (double) GeneFinder.readLength)/( (double) cluster.exonLength)) < GeneFinder.minCoverage){
									numGenesWithSupportBelowThreshold++;
								}
							}else{
								if(cluster.uniqueCov < GeneFinder.minCoverage){
									numGenesWithSupportBelowThreshold++;
								}
							}

						}

						if(((((double) cluster.idTOassociatedRnas.size()) * (double) GeneFinder.readLength)/( (double) cluster.exonLength)) < GeneFinder.minCoverage){
							numGenesWithTotalSupportBelowThreshold++;
							supportInfo = "n";
						}

						double coverage = ((((double) cluster.idTOassociatedRnas.size()) * (double) GeneFinder.readLength)/( (double) cluster.exonLength));
						
						int uniqueNum = (cluster.idTOassociatedRnas.keySet().size() - cluster.numOfMultis);

						if((((double)uniqueNum)/((double)cluster.idTOassociatedRnas.keySet().size()) < 0.1)){ // proportion of multi reads
							if((((double) uniqueNum) * (double) GeneFinder.readLength)/((double) cluster.exonLength) < 0.1){  // uniqueCov
								hasEnoughUniques = "n";
							}
						}


						//else{
						geneRefNonZero++;
						exonLength_covered_total += cluster.exonLength;  

						String strandInfo = "+";   // has to be updated if reverse strand

						if(!cluster.operonDirectionIsForward){
							strandInfo = "-";
						}

						if(cluster.operonOrfs.isEmpty()){
							// first write out transcript

							noCistron++;

							// CC ANA:

							bwGTF_ccAna.write(contigName + "\tGIIRA\ttranscript\t" + (cluster.startPos+1) + "\t" + (cluster.stopPos+1) + 
									"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

							// PROK FILE:

							bwGTF_prok.write(contigName + "\tGIIRA\tgene\t" + (cluster.startPos+1) + "\t" + (cluster.stopPos+1) + 
									"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

							// now write out all exons

							int exonNum = 1;
							for(int[] exon : cluster.exonsOfGene){
								bwGTF_ccAna.write(contigName + "\tGIIRA\texon\t" + (exon[0]+1) + "\t" + exon[1] + 
										"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; exon_number " + (exonNum++) + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");
							}

							exonNum = 1;

							for(int[] exon : cluster.exonsOfGene){
								bwGTF_prok.write(contigName + "\tGIIRA\tCDS\t" + (exon[0]+1) + "\t" + exon[1] + 
										"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; exon_number " + (exonNum++) + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");
							}


						} else if(cluster.operonOrfs.size() == 2){

							bwGTF_ccAna.write(contigName + "\tGIIRA\ttranscript\t" + (cluster.operonOrfs.get(1)[0]+1) + "\t" + (cluster.operonOrfs.get(1)[1]+1) + 
									"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

							bwGTF_ccAna.write(contigName + "\tGIIRA\texon\t" + (cluster.operonOrfs.get(1)[0]+1) + "\t" + (cluster.operonOrfs.get(1)[1]+1) + 
									"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; exon_number 1" + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

							bwGTF_prok.write(contigName + "\tGIIRA\tgene\t" + (cluster.startPos+1) + "\t" + (cluster.stopPos+1) + 
									"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

							bwGTF_prok.write(contigName + "\tGIIRA\tCDS\t" + (cluster.operonOrfs.get(1)[0]+1) + "\t" + (cluster.operonOrfs.get(1)[1]+1) + 
									"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; exon_number 1" + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

						} else{

							moreThanOneCistron++;
							if(cluster.operonOrfs.size() > 3){
								moreThanTwoCistrons++;
							}

							//manage 1/-1 for operon distinction
							
							if(cluster.operonOrfs.get(0)[0] == 1){
								bwGTF_prok.write(contigName + "\tGIIRA\tgene\t" + (cluster.startPos+1) + "\t" + (cluster.stopPos+1) + 
										"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");


								for(int i = 1; i<cluster.operonOrfs.size();++i){

									int[] orf = cluster.operonOrfs.get(i);

									bwGTF_ccAna.write(contigName + "\tGIIRA\ttranscript\t" + (orf[0]+1) + "\t" + (orf[1]+1) + 
											"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "_" + (i) + "\"; transcript_id \"Transcript." + cluster.geneID + "_" + (i) + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");


									bwGTF_ccAna.write(contigName + "\tGIIRA\texon\t" + (orf[0]+1) + "\t" + (orf[1]+1) + 
											"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "_" + (i) + "\"; transcript_id \"Transcript." + cluster.geneID + "_" + (i) + ".1\"; exon_number 1" + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

									bwGTF_prok.write(contigName + "\tGIIRA\tCDS\t" + (orf[0]+1) + "\t" + (orf[1]+1) + 
											"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "\"; transcript_id \"Transcript." + cluster.geneID + ".1\"; exon_number " + (i) + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

								}
							}else{
								moreOperons++;
								for(int i = 1; i<cluster.operonOrfs.size();++i){
									
									operonSplitNum++;
									
									if(cluster.operonOrfs.get(i)[0] == 0){
										strandInfo = "-";
									}else{
										strandInfo = "+";
									}
									
									bwGTF_prok.write(contigName + "\tGIIRA\tgene\t" + (cluster.operonOrfs.get(i)[1]+1) + "\t" + (cluster.operonOrfs.get(i)[2]+1) + 
											"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "_" + i + "\"; transcript_id \"Transcript." + cluster.geneID + "_"+ i + ".1" + "\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");


									int exonNum = 1;
									for(int posArr = 3; posArr<cluster.operonOrfs.get(i).length;++posArr){

										int[] orf = new int[2];
										orf[0] = cluster.operonOrfs.get(i)[posArr++];
										orf[1] = cluster.operonOrfs.get(i)[posArr];

										bwGTF_ccAna.write(contigName + "\tGIIRA\ttranscript\t" + (orf[0]+1) + "\t" + (orf[1]+1) + 
												"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "_" + i + "_" + (exonNum) + "\"; transcript_id \"Transcript." + cluster.geneID + "_" + i + "_" + (exonNum) + ".1\"; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");


										bwGTF_ccAna.write(contigName + "\tGIIRA\texon\t" + (orf[0]+1) + "\t" + (orf[1]+1) + 
												"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "_" + i + "_" + (exonNum) + "\"; transcript_id \"Transcript." + cluster.geneID + "_" + i + "_" + (exonNum) + ".1\"; exon_number 1" + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

										bwGTF_prok.write(contigName + "\tGIIRA\tCDS\t" + (orf[0]+1) + "\t" + (orf[1]+1) + 
												"\t" + cluster.score + "\t"+strandInfo+"\t0\tgene_id \"Gene." + cluster.geneID + "_" + i + "\"; transcript_id \"Transcript." + cluster.geneID + "_" + i + ".1\"; exon_number " + (exonNum++) + "; coverage: " + coverage + "; alsoUniqueSupport: " + multiInfo + "; coverageSupport: " + supportInfo + "; hasEnoughUniques: " + hasEnoughUniques + ";\n");

									}
									
								}
								
								operonSplitAv = ((double) operonSplitNum)/((double) moreOperons);
							}

						}

					}
				}

				if(GeneFinder.iteration == 1 && !GeneFinder.inprogeaCall){
					thisContig.allGenes = new Vector<Gene>();			
				}

			}

			//System.out.println("Number of prokaryotic genes without operon cistron: " +  noCistron);
			//System.out.println("Number of prokaryotic genes with more than one cistron: " +  moreThanOneCistron);
			//System.out.println("Number of prokaryotic genes with more than two cistrons: " +  moreThanTwoCistrons);
			//System.out.println("Number of identified genes with support below threshold: " +  numGenesWithSupportBelowThreshold);
			System.out.println("Number of identified transcripts with total coverage below threshold: " +  numGenesWithTotalSupportBelowThreshold);
			System.out.println("Number of identified transcripts with only ambiguous support: " +  numGenesWithOnlyMulti);
			System.out.println("Number of identified transcripts on reference: " +  geneRefNonZero);
			System.out.println("More than one operon in " +  moreOperons + " transcripts (with average operon number: " + operonSplitAv + ").");
		
			writeToLogFile("\n\nNumber of identified transcripts on reference: " +  geneRefNonZero + " \nNumber of identified transcripts with total coverage below threshold: " +  numGenesWithTotalSupportBelowThreshold + "\nNumber of identified transcripts with only ambiguous support: " +  numGenesWithOnlyMulti + "\n");

			bwGTF_ccAna.close();
			bwGTF_prok.close();
		}catch (FileNotFoundException r) {
			r.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
